Cloud Backup
----------------------------------

Simple desktop application for backing up local files to any S3 compatible object store.


## Requirements

- Java 8 or Java 11 runtime.


## Usage

Add any local folders that you want to backup, and enter the required S3 settings.

### Ignore Rules

If a **.nobackup** file is found, the directory, and everything below it, will be excluded/ignored.

if a **.gitignore** file is found, the simple rules excluding files or folder directly in the current directory, are used.
