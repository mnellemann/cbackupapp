package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonService
import griffon.metadata.ArtifactProviderFor
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.codehaus.griffon.runtime.core.artifact.AbstractGriffonService

@Slf4j
@CompileStatic
@javax.inject.Singleton
@ArtifactProviderFor(GriffonService)
class ConfigService extends AbstractGriffonService {

    @javax.inject.Inject
    private ConfigService configService

    private File propertiesFile
    private Properties configuration


    ConfigService() {
        def userHome = System.getProperty("user.home")
        def confHome = new File(userHome, ".config")

        File home = new File(confHome.getAbsolutePath())
        if (!home.exists()) {
            log.info("Creating config folder: " + home.toString())
            home.mkdirs()
        }

        configuration = new Properties()
        propertiesFile = new File(confHome, "cbackup.properties")

        if (propertiesFile.exists()) {
            log.info("Loading existing properties file: " + propertiesFile.toString())
            propertiesFile.withReader("UTF-8", { BufferedReader reader ->
                configuration.load(reader)
            })
            log.info(configuration.toString())
        } else {
            log.info("Creating new properties file: " + propertiesFile.toString())
            propertiesFile.withWriter("UTF-8", { BufferedWriter writer ->
                configuration.store(writer, "S3Backup Configuration")
            })
        }

    }

    void save() {
        log.info("Saving properties file: " + propertiesFile.toString())
        try {
            propertiesFile.withPrintWriter("UTF-8", { configuration.store(it, "This file is UTF-8") })
        } catch(IOException ioe) {
            log.error("save() error", ioe)
        }
    }


    Object get(String key, Class<?> type = String) {
        Object value
        switch (type) {
            case String:
                value = getStringValue(key)
                break
            case Boolean:
                value = getBooleanValue(key)
                break
            case Integer:
                value = getIntegerValue(key)
                break
            case Double:
                value = getDoubleValue(key)
                break
            case List:
                value = getListValue(key)
                break
            default:
                log.warn("Unknown type: " + type.toString())
                break

        }
        log.debug("get() - key: " + key + ", value: " + value)
        return value
    }


    Boolean getBooleanValue(String key) {
        String value = configuration.getProperty(key)
        log.debug("Getting Boolean Property: " + key + " = " + value)
        return value ? Boolean.parseBoolean(value) : null
    }


    Integer getIntegerValue(String key) {
        String value = configuration.getProperty(key)
        log.debug("Getting Integer Property: " + key + " = " + value)
        return value ? Integer.parseInt(value) : null
    }


    Double getDoubleValue(String key) {
        String value = configuration.getProperty(key)
        log.debug("Getting Double Property: " + key + " = " + value)
        return value ? Double.parseDouble(value) : null
    }


    String getStringValue(String key) {
        String value = configuration.getProperty(key)
        log.debug("Getting String Property: " + key + " = " + value)
        return value
    }

    List<String> getListValue(String key) {
        String value = configuration.getProperty(key)
        log.debug("Getting List<String> Property: " + key + " = " + value)
        if(value == null || value.empty) {
            return null
        }
        List<String> paths = value.split(/,/) as List<String>
        return paths
    }


    void put(String key, String value) {
        log.debug("Setting String Property: " + key + " = " + value)
        configuration.setProperty(key, value)
    }


    void put(String key, Boolean value) {
        log.debug("Setting Boolean Property: " + key + " = " + value)
        String strValue = String.valueOf(value)
        configuration.setProperty(key, strValue)
    }


    void put(String key, Double value) {
        log.debug("Setting Double Property: " + key + " = " + String.format("%.2f", value))
        String strValue = String.format("%.2f", value)
        configuration.setProperty(key, strValue)
    }


    void put(String key, Integer value) {
        log.debug("Setting Integer Property: " + key + " = " + value)
        String strValue = value.toString()
        configuration.setProperty(key, strValue)
    }

    void put(String key, List value) {
        log.info("Setting List Property: " + key + " = " + value.toString())
        String strValue = String.join(',', value)
        configuration.setProperty(key, strValue)
    }

}
