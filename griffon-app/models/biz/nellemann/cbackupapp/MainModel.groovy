package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonModel
import griffon.transform.Observable
import griffon.metadata.ArtifactProviderFor
import groovy.util.logging.Slf4j

@Slf4j
@ArtifactProviderFor(GriffonModel)
class MainModel {

    @javax.inject.Inject
    private ConfigService configService

    @Observable boolean running = false
    @Observable int clickCount = 0
    @Observable String errorMessage
    @Observable String lastMessage
    @Observable int eventCounter = 0

    // Main Settings
    List<String> includePaths
    List<String> excludeRules
    String endPoint
    String accessKey
    String secretKey
    String s3Bucket

    void load() {
        // Load Settings
        includePaths = configService.getListValue('includePaths')
        excludeRules = null
        endPoint = configService.get('s3Endpoint')
        accessKey = configService.get('s3AccessKey')
        secretKey = configService.get('s3SecretKey')
        s3Bucket = 's3backup-mark'
    }

    void mvcGroupInit(Map args) {
        log.info("mvcGroupInit()")
        load()
    }
}
