package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonModel
import griffon.metadata.ArtifactProviderFor
import griffon.transform.Observable
import groovy.util.logging.Slf4j

import javax.swing.DefaultListModel


@Slf4j
@ArtifactProviderFor(GriffonModel)
class SetupModel {

    @javax.inject.Inject
    private ConfigService configService

    // Application Options
    @Observable Boolean guiThemeDark

    // S3 Options
    @Observable String s3Endpoint
    @Observable String s3AccessKey
    @Observable String s3SecretKey

    @Observable DefaultListModel folderList = new DefaultListModel()
    int folderListSelectedIndex

    void save() {
        log.info("save()")

        configService.put('guiThemeDark', guiThemeDark)

        configService.put('s3Endpoint', s3Endpoint)
        configService.put('s3AccessKey', s3AccessKey)
        configService.put('s3SecretKey', s3SecretKey)

        configService.put('includePaths', folderList as List)

        configService.save()
    }


    void mvcGroupInit(Map args) {
        log.info("mvcGroupInit()")

        // Load Application Settings
        guiThemeDark = configService.getBooleanValue('guiThemeDark') ?: false

        // Load S3 Settings
        s3Endpoint = configService.getStringValue('s3Endpoint') ?: 'http://localhost:9000'
        s3AccessKey = configService.getStringValue('s3AccessKey') ?: 'AKIAIOSFODNN7EXAMPLE'
        s3SecretKey = configService.getStringValue('s3SecretKey') ?: 'wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY'

        List<String> tmpFolderList = configService.getListValue('includePaths') ?: [ '/home/mark/Documents' ]
        tmpFolderList.each { String str ->
            folderList.addElement(str)
        }
    }

}
