application {
    title = 'Cloud Backup'
    startupGroups = ['main']
    autoShutdown = true
}
mvcGroups {
    // MVC Group for "main"
    'main' {
        model      = 'biz.nellemann.cbackupapp.MainModel'
        view       = 'biz.nellemann.cbackupapp.MainView'
        controller = 'biz.nellemann.cbackupapp.MainController'
    }
    'setup' {
        model      = 'biz.nellemann.cbackupapp.SetupModel'
        view       = 'biz.nellemann.cbackupapp.SetupView'
        controller = 'biz.nellemann.cbackupapp.SetupController'
    }
}
