package biz.nellemann.cbackupapp

import biz.nellemann.cbackup.BackupEvent
import biz.nellemann.cbackup.BackupEventListener
import biz.nellemann.cbackup.MinioBackup
import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import groovy.util.logging.Slf4j

import javax.annotation.Nonnull

@Slf4j
@ArtifactProviderFor(GriffonController)
class MainController implements BackupEventListener {

    @MVCMember @Nonnull
    MainModel model


    @ControllerAction
    void click() {
        model.clickCount++
    }


    @ControllerAction
    void exit() {
        log.info("Application Shutdown")
        application.shutdown();
    }


    @ControllerAction
    void backup() {

        if(model.running) {
            return
        }

        model.load()    // Reload settings
        model.running = true
        model.eventCounter = 0
        log.info(model.includePaths.toString())


        MinioBackup minioBackup
        try {
            minioBackup = new MinioBackup(model.endPoint, model.accessKey, model.secretKey, model.s3Bucket, model.excludeRules);
            minioBackup.addEventListener(this)
        } catch(Exception e) {
            log.error("backup() - error: " + e)
            model.errorMessage = e.message
            return
        }

        for(String str : model.includePaths) {
            File includePath = new File(str)
            if(includePath.exists() && includePath.isDirectory()) {
                log.info("backup() - backing up " + includePath.toString())
                try {
                    minioBackup.uploadFolder(includePath)
                } catch(Exception e) {
                    log.error("backup() - error: " + e)
                    model.errorMessage = e.message
                    model.running = false
                    break
                }
            }
        }

        model.lastMessage = ''
        model.running = false
    }


    @ControllerAction
    void setup() {
        log.info("setup()")
        def params = [:]
        try {
            mvcGroup.createMVCGroup("setup", params)
        } catch(Exception e) {
            mvcGroup.destroyMVCGroup("setup")
        }
    }

    @ControllerAction
    void about() {
        log.info("about()")
    }


    @Override
    void onBackupEvent(BackupEvent event) {
        log.debug(event.message)
        model.eventCounter++
        model.lastMessage = event.message
    }

}
