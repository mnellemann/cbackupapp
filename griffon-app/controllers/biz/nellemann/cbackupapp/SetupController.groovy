package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonController
import griffon.core.controller.ControllerAction
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import groovy.util.logging.Slf4j

import javax.annotation.Nonnull

@Slf4j
@ArtifactProviderFor(GriffonController)
class SetupController {

    @MVCMember @Nonnull
    SetupModel model

    @MVCMember @Nonnull
    SetupView view

    @ControllerAction
    void cancel() {
        view.dialog.setVisible(false)
        mvcGroup.destroy()
    }

    @ControllerAction
    void guiThemeSystem() {
        log.info("guiThemeSystem()")
        model.guiThemeDark = false
    }

    @ControllerAction
    void guiThemeDark() {
        log.info("guiThemeDark()")
        model.guiThemeDark = true
    }

    @ControllerAction
    void save() {
        log.info("save()")

        model.s3Endpoint = view.s3Endpoint.text
        model.s3AccessKey = view.s3AccessKey.text
        model.s3SecretKey = view.s3SecretKey.text

        model.save()
        cancel()
    }

    @ControllerAction
    void addFolder() {
        log.info("addFolder()")
        File tmpFile = new File(view.inputFieldForFolder.text)
        if(tmpFile.exists() && tmpFile.isDirectory()) {
            model.folderList.addElement(view.inputFieldForFolder.text)
        }
    }

    @ControllerAction
    void delFolder() {
        log.info("delFolder()")
        if(model.folderListSelectedIndex) {
            model.folderList.remove(model.folderListSelectedIndex)
        }
    }

    void folderListSelect(int index) {
        log.info("folderListSelect()")
        model.folderListSelectedIndex = index
    }

}
