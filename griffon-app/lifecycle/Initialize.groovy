import biz.nellemann.cbackupapp.ConfigService
import com.github.weisj.darklaf.DarkLaf
import com.github.weisj.darklaf.LafManager
import com.github.weisj.darklaf.theme.DarculaTheme
import griffon.core.GriffonApplication
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.codehaus.griffon.runtime.core.AbstractLifecycleHandler

import javax.annotation.Nonnull
import javax.inject.Inject

import static griffon.util.GriffonApplicationUtils.isMacOSX
import static groovy.swing.SwingBuilder.lookAndFeel

@Slf4j
@CompileStatic
class Initialize extends AbstractLifecycleHandler {

    @javax.inject.Inject
    private ConfigService configService

    @Inject
    Initialize(@Nonnull GriffonApplication application) {
        super(application)
    }

    @Override
    void execute() {
        //lookAndFeel((isMacOSX ? 'system' : 'nimbus'), 'gtk', ['metal', [boldFonts: false]])

        Boolean darkTheme = configService.get('guiThemeDark', Boolean)
        log.debug("DarkTheme? " + darkTheme)
        if(darkTheme) {
            LafManager.setTheme(new DarculaTheme());
            lookAndFeel(DarkLaf.class.getCanonicalName())
        } else {
            lookAndFeel((isMacOSX ? 'system' : 'nimbus'), 'gtk', ['metal', [boldFonts: false]])
        }

    }
}
