package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor

import javax.swing.JProgressBar
import javax.annotation.Nonnull

@ArtifactProviderFor(GriffonView)
class MainView {

    @MVCMember @Nonnull
    FactoryBuilderSupport builder

    @MVCMember @Nonnull
    MainModel model

    JProgressBar progressBar

    void initUI() {
        builder.with {
            application(size: [400, 300], id: 'mainWindow', pack: true,
                resizable: true, locationByPlatform: true,
                title: application.configuration['application.title'],
                iconImage:   imageIcon('/griffon-icon-48x48.png').image,
                iconImages: [imageIcon('/griffon-icon-48x48.png').image,
                             imageIcon('/griffon-icon-32x32.png').image,
                             imageIcon('/griffon-icon-16x16.png').image]) {

                /*
                menuBar {
                    menu('File', mnemonic: 'F') {
                        menuItem(setupAction, label: 'Setup', mnemonic: 'S')
                        menuItem(exitAction, label: 'Exit', mnemonic: 'X')
                    }
                    separator()
                    menu('Help', mnemonic: 'H') {
                        menuItem(aboutAction, label: 'About', mnemonic: 'A')
                    }
                }*/

                migLayout(layoutConstraints: 'fill, insets dialog')

                /* -- CENTER --- */
                panel(constraints: 'grow') {
                    migLayout(layoutConstraints: 'insets panel, fill')

                    panel(constraints: 'wrap, align center') {
                        migLayout(layoutConstraints: 'fill')
                        button(backupAction, id: 'backupButton', label: 'Backup', constraints: 'sizegroup btns', enabled: bind { !model.running })
                        button(setupAction, id: 'setupButton', label: 'Setup', constraints: 'sizegroup btns', enabled: bind { !model.running })
                        button(exitAction, id: 'exitButton', label: 'Exit', constraints: 'sizegroup btns, wrap')
                    }

                    panel(constraints: 'wrap, align left') {
                        migLayout(layoutConstraints: 'fill')
                        label(text: 'Files proccessed:')
                        label(text: bind { model.eventCounter }, constraints: 'wrap')
                    }
                    label(text: bind { model.lastMessage }, constraints: 'wrap, grow, wmin 440')

                }

                /* -- SOUTH --- */
                panel(constraints: 'south, grow') {
                    migLayout(layoutConstraints: 'insets panel, fill')
                    progressBar = progressBar(indeterminate: true, value: bind { model.eventCounter }, visible: bind { model.running }, constraints: 'grow, wrap')
                    label(text: bind { model.errorMessage }, constraints: 'hmin 20')
                }

            }
        }
    }
}
