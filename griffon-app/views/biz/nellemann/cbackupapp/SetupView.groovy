package biz.nellemann.cbackupapp

import griffon.core.artifact.GriffonView
import griffon.inject.MVCMember
import griffon.metadata.ArtifactProviderFor
import groovy.util.logging.Slf4j
import net.miginfocom.swing.MigLayout

import javax.annotation.Nonnull
import javax.swing.*
import javax.swing.border.TitledBorder
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent

@Slf4j
@ArtifactProviderFor(GriffonView)
class SetupView {

    @MVCMember @Nonnull
    FactoryBuilderSupport builder

    @MVCMember @Nonnull
    SetupModel model

    @MVCMember @Nonnull
    SetupController controller

    JFrame mainFrame
    JDialog dialog

    JPanel mainPane
    JPanel remotePane
    JPanel localPane
    JPanel buttonsPane

    JTextField s3Endpoint
    JTextField s3AccessKey
    JTextField s3SecretKey

    JList folderList
    JTextField inputFieldForFolder

    void initUI() {

        log.info("initUI()")

        mainFrame = application.windowManager.findWindow("mainWindow")
        dialog = new JDialog(mainFrame, "Setup", true)

        mainPane = builder.panel {
            migLayout(layoutConstraints: 'fill, insets panel')
            panel(constraints: 'grow', border : titledBorder(title: "GUI Settings", border: etchedBorder(), titlePosition: TitledBorder.TOP)) {
                migLayout(layoutConstraints: '')

                label(text: "Look And Feel (requires restart)", constraints: 'wrap')

                buttonGroup(id: "themeGroup")
                radioButton(text: "System", selected: bind {!model.guiThemeDark}, buttonGroup: themeGroup, guiThemeSystemAction)
                radioButton(text: "Darcula", selected: bind {model.guiThemeDark}, buttonGroup: themeGroup, guiThemeDarkAction)

            }
        }

        remotePane = builder.panel {
            migLayout(layoutConstraints: 'fill, insets panel')
            panel(constraints: 'grow', border: titledBorder(title: "S3 Settings", border: etchedBorder(), titlePosition: TitledBorder.TOP)) {
                migLayout(layoutConstraints: '')

                label(text: "S3 Endpoint", constraints: '')
                s3Endpoint = textField(text: bind {model.s3Endpoint}, columns: 32, constraints: 'growx, wrap')

                label(text: "S3 Access Key", constraints: '')
                s3AccessKey = textField(text: bind {model.s3AccessKey}, columns: 32, constraints: 'growx, wrap')

                label(text: "S3 Secret Key", constraints: '')
                s3SecretKey = textField(text: bind {model.s3SecretKey}, columns: 32, constraints: 'growx, wrap')

            }
        }

        localPane = builder.panel {
            migLayout(layoutConstraints: 'fill, insets panel')
            panel(constraints: 'grow', border: titledBorder(title: "Backup Folders", border: etchedBorder(), titlePosition: TitledBorder.TOP)) {
                migLayout(layoutConstraints: '')

                scrollPane(constraints: 'grow, span 2, wrap') {
                    folderList = list(id: "folderList", model: bind { model.folderList })
                }
                button(delFolderAction, label: 'Remove Selected', constraints: 'growx, span 2, wrap')

                inputFieldForFolder = textField(columns: 32, constraints: 'growx')
                button(addFolderAction, label: 'Add', constraints: '')
            }

            // Detect mouse click in imageIconList
            folderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION)
            folderList.addMouseListener(new MouseAdapter() {
                @Override
                void mouseClicked(MouseEvent e) {
                    JList theList = (JList) e.getSource();
                    int index = theList.locationToIndex(e.getPoint());
                    if (index >= 0) {
                        Object o = theList.getModel().getElementAt(index);
                        //System.out.println("Double-clicked on: " + o.toString());
                        controller.folderListSelect(index)
                    }
                }
            })
        }

        buttonsPane = builder.panel {
            migLayout(layoutConstraints: 'nogrid')
            button(cancelAction, text: "Cancel", constraints: 'push, tag cancel, sizegroup bttn')
            button(saveAction, text: "Save", constraints: 'tag ok, sizegroup bttn' )
        }

    }


    void mvcGroupInit(Map<String,String> args) {

        log.info("mvcGroupInit()")

        JTabbedPane tabbedPane = new JTabbedPane()
        tabbedPane.addTab("Application", mainPane)
        tabbedPane.addTab("Cloud", remotePane)
        tabbedPane.addTab("Files", localPane)

        JPanel setupPanel = new JPanel()
        setupPanel.setLayout(new MigLayout('fill'))
        setupPanel.add(tabbedPane, 'north')
        setupPanel.add(buttonsPane, 'south')

        dialog.getContentPane().add(setupPanel)
        dialog.pack()
        dialog.setLocationRelativeTo(mainFrame)
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE)
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e) {
                mvcGroup.destroy()
            }
        })
        dialog.setVisible(true)
    }

}
